package graphics;

import java.util.concurrent.TimeUnit;

public class DroidTalker {
private String droid = "\n┈┈┈╲┈┈┈╱\n" +
        "┈┈╱▔▔╲\n" +
        "┈┃┈▇┈┈▇┈┃\n" +
        "╭╮┣━━━━━━┫╭╮\n" +
        "┃┃┃┈┈┈┈┈┈┃┃┃\n" +
        "╰╯┃┈┈┈┈┈┈┃╰╯\n" +
        "┈┈╰┓┏━━┓┏╯\n" +
        "┈┈┈╰╯┈┈╰╯\n";

    public void printDroidTalk(String text) throws InterruptedException {
        System.out.println(SimpleGraphicPrinter.ANSI_BLUE+this.droid);
        System.out.println("◤◤◤");
        for (char c : text.toCharArray()) {
            System.out.print(c);
            if ((c == '.') | (c == '!') | (c == '?')|(c == ';')) {
                System.out.println();
            }
            TimeUnit.MILLISECONDS.sleep(100);
        }
        System.out.println("◢◢◢"+SimpleGraphicPrinter.ANSI_RESET);
    }
}
