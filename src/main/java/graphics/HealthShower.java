package graphics;

public class HealthShower {
    private String full = "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬛";
    private String empty = "⬜⬜⬜⬜⬜⬜⬜⬜⬜⬜";

    public String showValue(int x, int max) {
        x = x * 100 / max;
        x = x / 10;
        if (x <= 0) return empty;
        if (x >= 10) return full;
        return switchBlock(x);
    }

    public String showValue(int x) {
        x = x / 10;
        if (x <= 0) return empty;
        if (x >= 10) return full;
        return switchBlock(x);
    }

    private String switchBlock(int x){
        switch (x) {
            case 1:
                return "⬛⬜⬜⬜⬜⬜⬜⬜⬜⬜";
            case 2:
                return "⬛⬛⬜⬜⬜⬜⬜⬜⬜⬜";
            case 3:
                return "⬛⬛⬛⬜⬜⬜⬜⬜⬜⬜";
            case 4:
                return "⬛⬛⬛⬛⬜⬜⬜⬜⬜⬜";
            case 5:
                return "⬛⬛⬛⬛⬛⬜⬜⬜⬜⬜";
            case 6:
                return "⬛⬛⬛⬛⬛⬛⬜⬜⬜⬜";
            case 7:
                return "⬛⬛⬛⬛⬛⬛⬛⬜⬜⬜";
            case 8:
                return "⬛⬛⬛⬛⬛⬛⬛⬛⬜⬜";
            case 9:
                return "⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜";
            default:
                return empty;
        }
    }
}

