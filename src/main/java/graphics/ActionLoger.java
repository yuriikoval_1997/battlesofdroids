package graphics;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class ActionLoger {
    private RandomColorGenerator randomColorGenerator = new RandomColorGenerator();

    private class RandomColorGenerator {
        private Random random = new Random(47);
        private String[] colors = {SimpleGraphicPrinter.ANSI_RED, SimpleGraphicPrinter.ANSI_GREEN,
                SimpleGraphicPrinter.ANSI_BLUE, SimpleGraphicPrinter.ANSI_CYAN,
                SimpleGraphicPrinter.ANSI_YELLOW, SimpleGraphicPrinter.ANSI_PURPLE};
        private String nextColor() {
            return colors[random.nextInt(6)];
        }
    }

    private void printSword() {
        System.out.println();
        System.out.println("    /\n" +
                "O===[====================-\n" +
                "    \\");
        System.out.println();
    }

    private void printColorText(String text) {
        System.out.print(randomColorGenerator.nextColor() + text + SimpleGraphicPrinter.ANSI_RESET);
    }

    public void printActionLoger(String log) throws InterruptedException {
        System.out.println();
        System.out.println("◤◤◤");
        this.printSword();
        for (Character c : log.toCharArray()) {
            this.printColorText(c.toString());
            if ((c == '.') | (c == '!') | (c == '?') | ((c == ';'))) {
                System.out.println();
            }
            TimeUnit.MILLISECONDS.sleep(100);
        }
        System.out.println("◢◢◢");
        System.out.println();
    }
}