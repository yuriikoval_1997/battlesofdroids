package graphics;

public class SimpleGraphicPrinter {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
    private static String eater = "\n" + "█████████ \n" +
            "█▄█████▄█ \n" +
            "█▼▼▼▼▼\n" +
            "█ \n" +
            "█▲▲▲▲▲\n" +
            "█████████\n" +
            "__██____██___" + "\n";
    private String helicopter = "  ▬▬▬.◙.▬▬▬\n" +
            "  ═▂▄▄▓▄▄▂\n" +
            "◢◤ █▀▀████▄▄▄▄▄▄◢◤\n" +
            "█▄  █     ██▀▀▀▀▀▀\n" +
            "◥███████◤\n" +
            "  ══╩══╩══";
    private static String droid = "\n┈┈┈╲┈┈┈╱\n" +
            "┈┈╱▔▔╲\n" +
            "┈┃┈▇┈┈▇┈┃\n" +
            "╭╮┣━━━━━━┫╭╮\n" +
            "┃┃┃┈┈┈┈┈┈┃┃┃\n" +
            "╰╯┃┈┈┈┈┈┈┃╰╯\n" +
            "┈┈╰┓┏━━┓┏╯\n" +
            "┈┈┈╰╯┈┈╰╯\n";
    private static String enemy = "     ,     ,\n" +
            "    (\\____/)\n" +
            "     (_oo_)\n" +
            "       (O)\n" +
            "     __||__    \\)\n" +
            "  []/______\\[] /\n" +
            "  / \\______/ \\/\n" +
            " /    /__\\\n" +
            "(\\   /____\\";

    private static void printSword() {
        System.out.println();
        System.out.println("    /\n" +
                "O===[====================-\n" +
                "    \\");
        System.out.println();
    }

    public static void printRedEater() {
        System.out.println(ANSI_RED + eater + ANSI_RESET);
    }

    public static void printGreenDroid() {
        System.out.println(ANSI_GREEN + droid + ANSI_RESET);
    }

    public static void printRedDroid() {
        System.out.println(ANSI_RED + enemy + ANSI_RESET);
    }
}
