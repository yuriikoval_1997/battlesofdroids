package graphics;

import java.util.concurrent.TimeUnit;

public class ScreenCleaner {
    public void printSnake(int n) throws InterruptedException {
        for (int i = 0; i <= n; i++) {
            System.out.println(SimpleGraphicPrinter.ANSI_PURPLE+"⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛"+SimpleGraphicPrinter.ANSI_RESET);
            TimeUnit.MILLISECONDS.sleep(50);
            System.out.println(SimpleGraphicPrinter.ANSI_RED+"⬛⬜⬛⬛⬛⬛⬛⬛⬛⬛"+SimpleGraphicPrinter.ANSI_RESET);
            TimeUnit.MILLISECONDS.sleep(50);
            System.out.println(SimpleGraphicPrinter.ANSI_CYAN+"⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛"+SimpleGraphicPrinter.ANSI_RESET);
            TimeUnit.MILLISECONDS.sleep(50);
            System.out.println(SimpleGraphicPrinter.ANSI_GREEN+"⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛"+SimpleGraphicPrinter.ANSI_RESET);
            TimeUnit.MILLISECONDS.sleep(50);
            System.out.println(SimpleGraphicPrinter.ANSI_BLUE+"⬛⬛⬛⬛⬜⬛⬛⬛⬛⬛"+SimpleGraphicPrinter.ANSI_RESET);
            TimeUnit.MILLISECONDS.sleep(50);
            System.out.println(SimpleGraphicPrinter.ANSI_YELLOW+"⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛"+SimpleGraphicPrinter.ANSI_RESET);
            TimeUnit.MILLISECONDS.sleep(50);
            System.out.println(SimpleGraphicPrinter.ANSI_WHITE+"⬛⬛⬛⬛⬛⬛⬜⬛⬛⬛"+SimpleGraphicPrinter.ANSI_RESET);
            TimeUnit.MILLISECONDS.sleep(50);
            System.out.println(SimpleGraphicPrinter.ANSI_PURPLE+"⬛⬛⬛⬛⬛⬛⬛⬜⬛⬛"+SimpleGraphicPrinter.ANSI_RESET);
            TimeUnit.MILLISECONDS.sleep(50);
            System.out.println(SimpleGraphicPrinter.ANSI_RED+"⬛⬛⬛⬛⬛⬛⬛⬛⬜⬛"+SimpleGraphicPrinter.ANSI_RESET);
            TimeUnit.MILLISECONDS.sleep(50);
            System.out.println(SimpleGraphicPrinter.ANSI_CYAN+"⬛⬛⬛⬛⬛⬛⬛⬛⬛⬜"+SimpleGraphicPrinter.ANSI_RESET);
            TimeUnit.MILLISECONDS.sleep(50);
            System.out.println("⬛⬛⬛⬛⬛⬛⬛⬛⬜⬛");
            TimeUnit.MILLISECONDS.sleep(50);
            System.out.println("⬛⬛⬛⬛⬛⬛⬛⬜⬛⬛");
            TimeUnit.MILLISECONDS.sleep(50);
            System.out.println("⬛⬛⬛⬛⬛⬛⬜⬛⬛⬛");
            TimeUnit.MILLISECONDS.sleep(50);
            System.out.println("⬛⬛⬛⬛⬛⬜⬛⬛⬛⬛");
            TimeUnit.MILLISECONDS.sleep(50);
            System.out.println("⬛⬛⬛⬛⬜⬛⬛⬛⬛⬛");
            TimeUnit.MILLISECONDS.sleep(50);
            System.out.println("⬛⬛⬛⬜⬛⬛⬛⬛⬛⬛");
            TimeUnit.MILLISECONDS.sleep(50);
            System.out.println("⬛⬛⬜⬛⬛⬛⬛⬛⬛⬛");
            TimeUnit.MILLISECONDS.sleep(50);
            System.out.println("⬛⬜⬛⬛⬛⬛⬛⬛⬛⬛");
            TimeUnit.MILLISECONDS.sleep(50);
            System.out.println("⬜⬛⬛⬛⬛⬛⬛⬛⬛⬛");
        }
    }
}
