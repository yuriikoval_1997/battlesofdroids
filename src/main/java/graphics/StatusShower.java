package graphics;

public class StatusShower {
    private SimpleMenuBuilder simpleMenuBuilder;
    private SimpleMenuBuilder simpleMenuBuilder2;
    private HealthShower healthShower;

    public StatusShower() {
        this.healthShower = new HealthShower();
        this.simpleMenuBuilder = new SimpleMenuBuilder();
        this.simpleMenuBuilder2 = new SimpleMenuBuilder();
    }

    public void showStatusScreen(int health, int energy, int markAttack, int markDefence) {
        simpleMenuBuilder.putMenuItems(" АТАКА", "1-голова", "2-корпус", "3-кінцівки");
        simpleMenuBuilder.drawItemAsSelected(markAttack);
        System.out.println(simpleMenuBuilder);
        simpleMenuBuilder2.putMenuItems("ЗАХИСТ", "1-голова", "2-корпус", "3-кінцівки");
        simpleMenuBuilder2.drawItemAsSelected(markDefence);
        System.out.println(simpleMenuBuilder2);
        HealthShower healthShower = new HealthShower();
        System.out.println("Життя: " + healthShower.showValue(health));
        System.out.println("Аккум: " + healthShower.showValue(energy));
    }

    public void resetMarked() {
        this.simpleMenuBuilder.drawAllItemsUnselected();
        this.simpleMenuBuilder2.drawAllItemsUnselected();
    }
}
