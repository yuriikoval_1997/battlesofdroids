package graphics;

import game_play.armor.ArmorWrapper;
import game_play.weapons.WeaponWrapper;

import java.util.Map;
import java.util.Scanner;

public class SimpleMenuBuilder {
    private int itemCounter = 0;
    private StringBuilder menu;

    @Override
    public String toString() {
        return menu.toString();
    }

    public void putMenuItems(String menuCaption, String... menuItems) {
        menu = new StringBuilder();
        menu.append("████[ " + menuCaption + " ]████\n");
        for (String s : menuItems) {
            itemCounter++;
            menu.append("⬜ - " + s + "\n");
        }
    }

    public void putArmorMenuItems(String menucaption, Map<Integer, ArmorWrapper> map) {
        menu = new StringBuilder();
        menu.append("████[ " + menucaption + " ]████\n");
        for (Map.Entry<Integer,ArmorWrapper> s : map.entrySet()) {
            itemCounter++;
            menu.append("⬜ - " +s.getValue().getArmor()+" "+s.getValue().getPrice()+"( enter "+s.getKey()+")"+"\n");
        }
    }

    public void putWeaponMenuItemsW(String menuCaption, Map<Integer, WeaponWrapper> map) {
        menu = new StringBuilder();
        menu.append("████[ " + menuCaption + " ]████\n");
        for (Map.Entry<Integer,WeaponWrapper> s : map.entrySet()) {
            itemCounter++;
            menu.append("⬜ - ").append(s.getValue().getWeapon()).append(" ")
                    .append(s.getValue().getPrice()).append("( enter ").append(s.getKey()).append(")").append("\n");
        }
    }

    public void drawItemAsSelected(int index) {
        java.util.Scanner sc = new Scanner(this.menu.toString());
        sc.useDelimiter("\n");
        StringBuilder newmenu = new StringBuilder();
        int flag = 0;
        while (sc.hasNext()) {
            if (flag == index) {
                newmenu.append(sc.next().replaceAll("⬜", "⬛"));
                newmenu.append("\n");
            }
            if (sc.hasNext()) {
                newmenu.append(sc.next());
                newmenu.append("\n");
            }
            flag++;
        }
        this.menu = newmenu;
    }

    public void drawAllItemsUnselected() {
        String rez = this.menu.toString().replaceAll("⬛", "⬜");
        this.menu = new StringBuilder(rez);
    }
}