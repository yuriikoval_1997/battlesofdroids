package graphics;

public class Demo {

    public static void main(String[] args) throws Exception {
        // Просто замість заставки
        ScreenCleaner screenCleaner = new ScreenCleaner();
        // Передайте в метод інтове число, щоб продовжити чи скоротити анімацію, використовую замісь очещення екрану
        screenCleaner.printSnake(5);
        // Клас із відображення реплік нашого героя
        DroidTalker droidTalker = new DroidTalker();
        // Закінчуйте речення в аргументі цього метода знаками "!","?",";","." Інакше паганий аутпут!
        droidTalker.printDroidTalk("Я Дроід! Я можу говорити! Це можна застосувати як заставку; " +
                "Дякую за увагу!");
        EnemyDroidTalker enemyDroidTalker = new EnemyDroidTalker();
        enemyDroidTalker.printDroidTalk("Я ворожий дроід! Я можу сказати багато поганих слів. " +
                "Дякую за увагу?");
        // Логер для виведення логу драки.
        ActionLoger actionLoger = new ActionLoger();
        // Згодуйте методу стрінгу з логом. Закінчуйте речення в аргументі цього метода знаками "!","?",";","." Інакше паганий аутпут!

        actionLoger.printActionLoger("Ваш дроід атакує: ГОЛОВА; УДАЧА! Нанесений урон: 20. Енергія: -20.");
        actionLoger.printActionLoger("Ворожий дроід атакує: КОРПУС; ЗАБЛОКОВАНО! Нанесений урон: 0. Енергія: -20. dfdfdfdfdfdf. dfddfdffdfd dfdfdfdfdfffdf! ddfdffeffefef!");



        SimpleMenuBuilder simpleMenuBuilder = new SimpleMenuBuilder();
        simpleMenuBuilder.putMenuItems("Нове меню","пункт 1","пункт 2", "пункт 3","пункт 4","пункт 5","пункт 6","пункт 7");
        System.out.println(simpleMenuBuilder);
        simpleMenuBuilder.drawItemAsSelected(4);

        System.out.println(SimpleGraphicPrinter.ANSI_GREEN);
        System.out.println(simpleMenuBuilder);
        System.out.println(SimpleGraphicPrinter.ANSI_RESET);

        System.out.println(SimpleGraphicPrinter.ANSI_CYAN);
        simpleMenuBuilder.drawAllItemsUnselected();
        System.out.println(simpleMenuBuilder);
        System.out.println(SimpleGraphicPrinter.ANSI_RESET);

        HealthShower healthShower = new HealthShower();
        System.out.println("Здоровя: "+healthShower.showValue(4,10));

    }
}
