package graphics;

import java.util.concurrent.TimeUnit;

public class InputPicker {
    private static String line = "═";

    public InputPicker() {
    }

    public void getInput(String caption) throws InterruptedException {
        System.out.print(caption + "]");
        for (int i = 0; i <= caption.length(); i++) {
            System.out.print(line);
            TimeUnit.MILLISECONDS.sleep(75);
        }
        System.out.print(">>>");
    }
}
