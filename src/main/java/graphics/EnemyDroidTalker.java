package graphics;

import java.util.concurrent.TimeUnit;

public class EnemyDroidTalker {
    private String enemy = "     ,     ,\n" +
            "    (\\____/)\n" +
            "     (_oo_)\n" +
            "       (O)\n" +
            "     __||__    \\)\n" +
            "  []/______\\[] /\n" +
            "  / \\______/ \\/\n" +
            " /    /__\\\n" +
            "(\\   /____\\";

    public void printDroidTalk(String text) throws InterruptedException {
        System.out.println(SimpleGraphicPrinter.ANSI_RED+this.enemy);
        System.out.println("◤◤◤");
        for (char c : text.toCharArray()) {
            System.out.print(c);
            if ((c == '.') | (c == '!') | (c == '?')) {
                System.out.println();
            }
            TimeUnit.MILLISECONDS.sleep(100);
        }
        System.out.println("◢◢◢");
        System.out.println(SimpleGraphicPrinter.ANSI_RESET);
    }
}
