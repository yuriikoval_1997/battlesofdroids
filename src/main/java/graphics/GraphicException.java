package graphics;

public class GraphicException extends RuntimeException {
    private String eater = "\n" + "█████████ \n" +
            "█▄█████▄█ \n" +
            "█▼▼▼▼▼\n" +
            "█ \n" +
            "█▲▲▲▲▲\n" +
            "█████████\n" +
            "__██____██___" + "\n";

    @Override
    public String getMessage() {
        return this.eater + "\n"+super.getMessage();
    }
}
