package game_play.health;

public class Health {
    private int head;
    private int body;
    private int arms;
    private int legs;

    public Health(){
        this.head=25;
        this.body=25;
        this.arms=25;
        this.legs=25;
    }

    public int ofHead(){
        return this.head;
    }
    public int ofBody(){
        return this.body;
    }
    public int ofArms(){
        return this.arms;
    }
    public int ofLegs(){
        return this.legs;
    }

    public int sumOf (){ return this.head+this.body+this.arms+this.legs; }

    public int getBodyPartHP(String bodyPart){
        switch(bodyPart){
            case "Head":
                return this.head;
            case "Body":
                return this.body;
            case "Arms":
                return this.arms;
            case "Legs":
                return this.legs;
            default: return -1;
        }
    }

    public void setBodyPartHP(String bodyPart, int healthPoints){
        switch(bodyPart){
            case "Head":
                this.head = healthPoints;
                if(this.head<0)
                    this.head=0;
            break;
            case "Body":
                this.body = healthPoints;
                if(this.body<0)
                    this.body=0;
            break;
            case "Arms":
                this.arms = healthPoints;
                if(this.arms<0)
                    this.arms=0;
            break;
            case "Legs":
                this.legs = healthPoints;
                if(this.legs<0)
                    this.legs=0;
            break;
            default:
        }
    }
    public void setHeadHP(int head){
        this.head=head;
        if(this.head<0)
            this.head=0;
    }
    public void setBodyHP(int body){
        this.body=body;
        if(this.body<0)
            this.body=0;
    }
    public void setArmsHP(int arms){
        this.arms=arms;
        if(this.arms<0)
            this.arms=0;
    }
    public void setLegsHP(int legs){
        this.legs=legs;
        if(this.legs<0)
            this.legs=0;
    }
}
