package game_play.entrance;

import game_play.exceptions.NotEnoughMoneyException;
import game_play.armor.*;
import game_play.droids.Player;
import game_play.money.Money;
import game_play.weapons.*;
import graphics.SimpleGraphicPrinter;
import graphics.SimpleMenuBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.TreeMap;

public class Market {
    private static Market market;
    private TreeMap<Integer, ArmorWrapper> armorStorage = new TreeMap<>();
    private TreeMap<Integer, WeaponWrapper> weaponStorage = new TreeMap<>();
    private int rechargingDroidBatteryPrice = 10;
    private int rechargingEnergyShieldPrice = 20;

    {
        armorStorage.put(1, new ArmorWrapper(new SimpleArmor(), 30));
        armorStorage.put(2, new ArmorWrapper(new MandalorianArmor(), 75));
        armorStorage.put(3, new ArmorWrapper(new EnergyShield(), 130));
        weaponStorage.put(1, new WeaponWrapper(new Sword(), 30));
        weaponStorage.put(2, new WeaponWrapper(new Axe(), 80));
        weaponStorage.put(3, new WeaponWrapper(new Blaster(), 250));
        weaponStorage.put(4, new WeaponWrapper(new GaussRifle(), 550));
    }

    private Market(){}

    public static synchronized Market getInstance(){
        if (market == null){
            market = new Market();
        }
        return market;
    }

    static void marketGreeting(){
        System.out.println(SimpleGraphicPrinter.ANSI_CYAN+"Welcome to our shop"+"\n");
    }

    static void marketFarewell(){
        System.out.println("▇▇EXIT FROM SHOP▇▇");
        System.out.println(SimpleGraphicPrinter.ANSI_RESET);
    }

    /**
     * This methods prints armor price-list.
     */
    void printArmorPrice(){
        SimpleMenuBuilder simpleMenuBuilder = new SimpleMenuBuilder();
        simpleMenuBuilder.putArmorMenuItems("Armor price list",armorStorage);
        System.out.println(simpleMenuBuilder);
    }

    /**
     * This methods prints weapons price-list.
     */
    void printWeaponsPrice(){
     SimpleMenuBuilder simpleMenuBuilder = new SimpleMenuBuilder();
     simpleMenuBuilder.putWeaponMenuItemsW("Weapon price list",weaponStorage);
     System.out.println(simpleMenuBuilder);
    }

    void printRechargingPrice(){
        System.out.printf("To recharge Droid's battery costs %d%s (Enter 101)",
                rechargingDroidBatteryPrice, Money.DOLLAR);
        System.out.println();
        System.out.printf("To recharge Energy shield's battery costs %d%s (Enter 102)",
                rechargingEnergyShieldPrice, Money.DOLLAR);
        System.out.println();
    }

    void sell(Player customer, BufferedReader consoleReader) throws IOException{
        sellArmor(customer, consoleReader);
        sellWeapon(customer, consoleReader);
    }

    private void sellArmor(Player customer, BufferedReader consoleReader) throws IOException{
        int armorId;
        while (true){
            try {
                System.out.println("Enter a corresponding number to buy armor or 0 if you don't want to buy any armor.");
                armorId = Integer.parseInt(consoleReader.readLine());
                if (armorId == 0) {
                    break;
                } else if (armorStorage.containsKey(armorId)) {
                    ArmorWrapper armorWrapper = armorStorage.get(armorId);
                    if (customer.getMoney() < armorWrapper.getPrice()) {
                        throw new NotEnoughMoneyException();
                    }
                    customer.spendMoney(armorWrapper.getPrice());
                    Armor armor = armorWrapper.getArmor();
                    customer.getArmorItems().put(armorId, armor);
                    System.out.printf("You've just bought %s. Now you have %d%s left.", armor, customer.getMoney(),
                            Money.DOLLAR);
                    System.out.println();
                } else {
                    throw new NoSuchElementException();
                }
            } catch (NumberFormatException e){
                System.out.println("You have entered not a number! ");
            } catch (NoSuchElementException e){
                System.out.println("There is no such armor!");
            }
        }
    }

    private void sellWeapon(Player customer, BufferedReader consoleReader) throws IOException {
        int weaponId;
        while (true){
            try {
                System.out.println("Enter a corresponding number to buy weapon " +
                        "or 0 if you don't want to buy any weapons.");
                weaponId = Integer.parseInt(consoleReader.readLine());
                if (weaponId == 0) {
                    break;
                } else if (weaponStorage.containsKey(weaponId)) {
                    WeaponWrapper weaponWrapper = weaponStorage.get(weaponId);
                    if (customer.getMoney() < weaponWrapper.getPrice()) {
                        throw new NotEnoughMoneyException();
                    }
                    customer.spendMoney(weaponWrapper.getPrice());
                    Weapon weapon = weaponWrapper.getWeapon();
                    customer.getWeaponItems().put(weaponId, weapon);
                    System.out.printf("You've just bought %s. Now you have %d%s left.", weapon, customer.getMoney(),
                            Money.DOLLAR);
                    System.out.println();
                } else {
                    throw new NoSuchElementException();
                }
            } catch (NumberFormatException e){
                System.out.println("You have entered not a number!");
            } catch (NoSuchElementException e){
                System.out.println("There is no such a weapon!");
            }
        }
    }

    void recharge(Player customer, BufferedReader consoleReader) throws InterruptedException, IOException{
        rechargeDroidBattery(customer, consoleReader);
        rechargeEnergyShield(customer, consoleReader);
    }

    private void rechargeDroidBattery(Player customer, BufferedReader consoleReader)
            throws InterruptedException, IOException {
        System.out.println("Enter a corresponding number to recharge Droid's " +
                "battery or 0 if you don't want to recharge at all.");
        if (! askIfCustomerWantsToRecharge(consoleReader, 101)){
            return;
        }
        if (customer.getMoney() < rechargingDroidBatteryPrice){
            System.out.println("You don't have enough money.");
            return;
        }
        if (customer.getBattery().getEnergy() == 100){
            System.out.println("Your droid's battery is full!");
            return;
        }
        customer.spendMoney(rechargingDroidBatteryPrice);
        customer.getBattery().recharge();
    }

    private void rechargeEnergyShield(Player customer, BufferedReader consoleReader)
            throws InterruptedException, IOException{
        System.out.println("Enter a corresponding number to recharge Energy shield's battery " +
                "battery or 0 if you don't want to recharge at all.");
        if (! askIfCustomerWantsToRecharge(consoleReader, 102)){
            return;
        }
        if (customer.getMoney() < rechargingEnergyShieldPrice){
            System.out.println("You don't have enough money.");
            return;
        }
        EnergyShield energyShield;
        for (Map.Entry<Integer, Armor> entry : customer.getArmorItems().entrySet()){
            if (entry.getValue() instanceof EnergyShield){
                energyShield = (EnergyShield) entry.getValue();
                if (energyShield.getShieldBattery().getEnergy() == 100){
                    System.out.println("Your EnergyShield Battery is full!");
                }
                energyShield.getShieldBattery().recharge();
                return;
            }
        }
        System.out.println("You don't have any Energy shield to recharge!");
    }

    private boolean askIfCustomerWantsToRecharge(BufferedReader consoleReader, int id) throws IOException {
        int input;
        while (true) {
            try {
                input = Integer.parseInt(consoleReader.readLine());
                if (input == 0){
                    return false;
                }
                if (input == id){
                    break;
                }
                throw new NoSuchElementException();
            } catch (NumberFormatException e) {
                System.out.println("You have entered not a number!");
            } catch (NoSuchElementException e) {
                System.out.println("There is no such thing to recharge!");
            }
        }
        return true;
    }
}
