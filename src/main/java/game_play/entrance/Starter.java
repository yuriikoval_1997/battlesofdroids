package game_play.entrance;

import game_play.droids.BotEnemy;
import game_play.droids.Droid;
import game_play.droids.Player;
import game_play.money.Money;
import game_play.weapons.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import graphics.*;

public class Starter {
    /**
     * This field keeps names of the bot-droids.
     */
    private static TreeMap<Integer, String> botNames = new TreeMap<>();
    private static TreeMap<Integer, Weapon> availableWeapons = new TreeMap<>();

    static {
        botNames.put(0, "R2-D2");
        botNames.put(1, "T-600");
        botNames.put(2, "Bender-2999");
        botNames.put(3, "Devil-BotEnemy");
        botNames.put(4, "T-800");
        botNames.put(5, "T-1000");
    }

    static {
        availableWeapons.put(0, new Sword());
        availableWeapons.put(1, new Axe());
        availableWeapons.put(2, new Blaster());
    }

    public static void main(String[] args) {
        try (BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in))) {
            boolean ifContinue = playScreensaver(consoleReader);
            if (ifContinue) {
                boolean playAgain;
                do {
                    InputPicker inputPicker = new InputPicker();
                    inputPicker.getInput("Enter user's name: ");
                    String userName = consoleReader.readLine();
                    playAgain = play(userName, consoleReader);
                } while (playAgain);

            } else {
                System.out.println("Only a pathetic coward can run away from a battle!");
            }
        } catch (Exception e) {
            SimpleGraphicPrinter.printRedEater();
            System.err.println("If many things can go wrong, " +
                    "they all will go wrong at the same time. - Murphy's law.");
            /*TODO - replace e.printStackTrace before the release.
            Because of safety reasons you cannot use this e.printStackTrace() in a real project,
            but it's ok to use it for debugging.
             */
            e.printStackTrace();
        }
    }

    /**
     * This method shows the introduction to the game and asks if a user wants to play.
     *
     * @param consoleReader is given to playScreensaver
     * @return askIfUserWantsToPlay().
     * @throws IOException if askIfUserWantsToPlay() throws an IOException.
     */
    private static boolean playScreensaver(BufferedReader consoleReader) throws IOException, InterruptedException {
        ScreenCleaner screenCleaner = new ScreenCleaner();
        DroidTalker droidTalker = new DroidTalker();
        EnemyDroidTalker enemyDroidTalker = new EnemyDroidTalker();
        screenCleaner.printSnake(4);
        droidTalker.printDroidTalk("I'm a new droid on this Droids arena, and I'm gonna kick some asses!");
        screenCleaner.printSnake(2);
        enemyDroidTalker.printDroidTalk("You are a pathetic rusty canister and I'll crush you!");
        screenCleaner.printSnake(2);
        return askIfUserWantsToPlay(consoleReader);
    }

    /**
     * Here takes place the main game-play.
     * @param consoleReader is given to askIfUserWantsToPlay().
     * @param userName      is used to create an object of Player.
     * @throws IOException if askIfUserWantsToPlay() throws an IOException.
     */
    private static boolean play(String userName, BufferedReader consoleReader) throws IOException, InterruptedException {
        Droid.zeroLvl();
        Player player = new Player(userName);
        boolean victory;
        do {
            goShopping(player, consoleReader);
            BotEnemy botEnemy = new BotEnemy(getRandomBotName());
            botEnemy.giveWeapon(getRandomBotWeapon());
            victory = goFighting(player, botEnemy, consoleReader);
            if (victory){
                player.addMoney(100);
            } else {
                break;
            }
        }
        while (true);
        System.out.printf("You have fought well and earned %d%s", player.getMoney(), Money.DOLLAR);
        System.out.println();
        System.out.println("Would you like to play again?");
        return askIfUserWantsToPlay(consoleReader);
    }

    /**
     * It asks the user to enter "yes" into the console if he/she wants to play, otherwise - "no".
     * If the user has entered "no", the method returns false as a stop sign for the program.
     * INPUT IS CASE INSENSITIVE!
     *
     * @param consoleReader is used to ask the user to enter "yes" or "no".
     * @return true if the user entered "yes", false if the user entered "no".
     * Otherwise - continues until correct input is entered.
     * @throws IOException if the methods of consoleReader throw an IOException.
     */
    private static boolean askIfUserWantsToPlay(BufferedReader consoleReader) throws IOException, InterruptedException {
        InputPicker inputPicker = new InputPicker();
        String input;
        do {
            inputPicker.getInput("Lets Play? 'yes'/'no'");
            input = consoleReader.readLine().toLowerCase();
            if (!input.equals("yes") && !input.equals("no")) {
                SimpleGraphicPrinter.printRedEater();
                System.out.println("You have entered invalid input. Try again!");
            } else {
                break;
            }
        } while (true);
        return input.equals("yes");
    }

    /**
     * This method returns random value out of the botNames map.
     * @return random bot name.
     */
    private static String getRandomBotName() {
        Random random = new Random();
        int randomInt = random.nextInt(botNames.size());
        return botNames.get(randomInt);
    }
    /**
     * This method returns random value out of the availableWeapons map.
     *
     * @return random weapon.
     */
    private static Weapon getRandomBotWeapon(){
        Random random = new Random();
        int randomInt = random.nextInt(availableWeapons.size());
        return availableWeapons.get(randomInt);
    }

    /**
     * This method represent shopping functionality.
     * @param player is an object that represent the human-player
     * @param consoleReader for interaction with the shop.
     * @throws IOException if consoleReader throws one.
     */
    private static void goShopping(Player player, BufferedReader consoleReader) throws IOException, InterruptedException {
        printStatus(player);
        Market market = Market.getInstance();
        Market.marketGreeting();
        market.printArmorPrice();
        market.printWeaponsPrice();
        market.printRechargingPrice();
        market.sell(player, consoleReader);
        market.recharge(player, consoleReader);
        Market.marketFarewell();
    }

    /**
     * This method shows all information about the player.
     * @param player is an object that represent the human-player
     */
    private static void printStatus(Player player) {
        HealthShower healthShower = new HealthShower();
        SimpleGraphicPrinter.printGreenDroid();
        System.out.println("Level " + Player.getLvl());
        System.out.println(SimpleGraphicPrinter.ANSI_RED+ "Head integrity: " + healthShower.showValue(player.healthPoints.ofHead(), 25)
        +SimpleGraphicPrinter.ANSI_RESET);
        System.out.println(SimpleGraphicPrinter.ANSI_RED+ "Body integrity: " + healthShower.showValue(player.healthPoints.ofBody(), 25)
                +SimpleGraphicPrinter.ANSI_RESET);
        System.out.println(SimpleGraphicPrinter.ANSI_RED+ "Arms integrity: " + healthShower.showValue(player.healthPoints.ofArms(), 25)
                +SimpleGraphicPrinter.ANSI_RESET);
        System.out.println(SimpleGraphicPrinter.ANSI_RED+ "Legs integrity: " + healthShower.showValue(player.healthPoints.ofLegs(), 25)
                +SimpleGraphicPrinter.ANSI_RESET);
        System.out.println(SimpleGraphicPrinter.ANSI_PURPLE+"Energy: " + healthShower.showValue(player.getBattery().getEnergy())
        +SimpleGraphicPrinter.ANSI_RESET);
        System.out.println("Money " + player.getMoney() + Money.DOLLAR);
        System.out.println("Armor: ");
        if (player.getArmorItems().size() == 0) {
            System.out.println(" You don't nave any armor.");
        } else {
            player.getArmorItems().forEach((k, v) -> System.out.println("    " + k + " - " + v));
            System.out.println();
        }
        System.out.println("Weapon: ");
        if (player.getWeaponItems().size() == 0) {
            System.out.println(" You don't nave any weapons.");
        } else {
            player.getWeaponItems().forEach((k, v) -> System.out.println("    " + k + " - " + v));
            System.out.println();
            System.out.println();
        }
    }

    /**
     * This method shows status of bot-enemy.
     * @param enemy represent bot.
     */
    private static void printEnemyStatus(BotEnemy enemy){
        HealthShower healthShower = new HealthShower();
        SimpleGraphicPrinter.printRedDroid();
        System.out.println(SimpleGraphicPrinter.ANSI_RED+ "Head integrity: " + healthShower.showValue(enemy.healthPoints.ofHead(), 25)
                +SimpleGraphicPrinter.ANSI_RESET);
        System.out.println(SimpleGraphicPrinter.ANSI_RED+ "Body integrity: " + healthShower.showValue(enemy.healthPoints.ofBody(), 25)
                +SimpleGraphicPrinter.ANSI_RESET);
        System.out.println(SimpleGraphicPrinter.ANSI_RED+ "Arms integrity: " + healthShower.showValue(enemy.healthPoints.ofArms(), 25)
                +SimpleGraphicPrinter.ANSI_RESET);
        System.out.println(SimpleGraphicPrinter.ANSI_RED+ "Legs integrity: " + healthShower.showValue(enemy.healthPoints.ofLegs(), 25)
                +SimpleGraphicPrinter.ANSI_RESET);
        System.out.println("Weapon: ");
            System.out.println(enemy.getWeapon());
    }

    private static boolean goFighting(Player player, BotEnemy enemy, BufferedReader consoleReader) throws IOException, InterruptedException {
        String bodyPart;
        String randomBodyPart;
        Random random = new Random();
        int randomIntBodyPart;
        //Fighting cycle
        do {
            //Random target body part chosen for enemy bot
            randomIntBodyPart = random.nextInt(4) + 1;
            switch (randomIntBodyPart) {
                case 1:
                    randomBodyPart = "Head";
                    break;
                case 2:
                    randomBodyPart = "Body";
                    break;
                case 3:
                    randomBodyPart = "Arms";
                    break;
                case 4:
                    randomBodyPart = "Legs";
                    break;
                default:
                    randomBodyPart = "Head";
            }
            //Target body part chosen by player
            System.out.println(SimpleGraphicPrinter.ANSI_RED + " Choose where to hit: "
                    + SimpleGraphicPrinter.ANSI_RESET);
            SimpleMenuBuilder simpleMenuBuilder = new SimpleMenuBuilder();
            simpleMenuBuilder.putMenuItems("TARGETS","Head (Enter 1)","Body (Enter 2)",
                    "Arms (Enter 3)","Legs (Enter 4)");
            System.out.println(SimpleGraphicPrinter.ANSI_GREEN+simpleMenuBuilder+SimpleGraphicPrinter.ANSI_RESET);
            bodyPart = consoleReader.readLine();
            //Amplify damage with every weapon that player has and reduce players energy
            if(player.healthPoints.ofArms()>0){
                if (player.getWeaponItems().size() != 0) {
                    for (Map.Entry<Integer, Weapon> weaponEntry : player.getWeaponItems().entrySet()) {
                        player.shoot(enemy, bodyPart, weaponEntry.getValue().getDamage());
                        player.getBattery().power(weaponEntry.getValue().getEnergyCost());
                    }
                }
            }
            //Enemy's turn
            if(enemy.healthPoints.ofBody()>0&&enemy.healthPoints.ofArms()>0)
             //   if (player.getArmorItems()!=0)
                enemy.shoot(player, randomBodyPart, enemy.getWeapon().getDamage());
            printStatus(player);
            printEnemyStatus(enemy);
        } while (player.getHealthPoints() > 0 && player.getBattery().getEnergy() > 0 && enemy.getHealthPoints() > 0 && player.healthPoints.ofBody()>0 && enemy.healthPoints.ofBody()>0);
        return (player.healthPoints.ofBody() > 0) && (player.getBattery().getEnergy() > 0);
    }
}
