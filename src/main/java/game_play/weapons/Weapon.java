package game_play.weapons;

import game_play.droids.Droid;

public abstract class Weapon implements Harmful {
    private int damage;
    private int energyCost;

    Weapon(int damage, int energyCost) {
        this.damage = damage;
        this.energyCost = energyCost;
    }

    @Override
    public void causeDamage(Droid enemy, String bodyPart){
        enemy.hurt(this.damage, bodyPart);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    @Override
    public int hashCode() {
        return this.getClass().getSimpleName().hashCode();
    }

    public int getDamage(){
        return this.damage;
    }

    public int getEnergyCost(){
        return this.energyCost;
    }
}
