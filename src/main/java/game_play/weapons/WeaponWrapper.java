package game_play.weapons;

public class WeaponWrapper {
    private Weapon weapon;
    private int price;

    public WeaponWrapper(Weapon weapon, int price) {
        this.weapon = weapon;
        this.price = price;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public int getPrice() {
        return price;
    }
}
