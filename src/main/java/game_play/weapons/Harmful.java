package game_play.weapons;

import game_play.droids.Droid;

public interface Harmful {
    void causeDamage(Droid enemy, String bodyPart);
}
