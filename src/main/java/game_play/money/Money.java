package game_play.money;

public enum Money {
    HRIVNA("₴"),
    DOLLAR("$"),
    EURO("€"),
    POUND("£"),
    YEN("¥");

    private String currency;

    Money(String currency){
        this.currency = currency;
    }

    @Override
    public String toString() {
        return currency;
    }
}
