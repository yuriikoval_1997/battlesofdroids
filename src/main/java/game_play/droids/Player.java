package game_play.droids;

import game_play.armor.Armor;
import game_play.weapons.Weapon;
import graphics.ActionLoger;

import java.util.TreeMap;

public class Player extends Droid {
    private int money;
    private TreeMap<Integer, Armor> armorItems = new TreeMap<>();
    private TreeMap<Integer, Weapon> weaponItems = new TreeMap<>();

    public Player(String name)  {
        super(name);
        this.money = 100;
    }

    public int getMoney() {
        return money;
    }

    public void addMoney(int bounty) {
        this.money +=bounty;
    }

    public void spendMoney(int price){
        this.money -=price;
    }

    public TreeMap<Integer, Armor> getArmorItems() {
        return armorItems;
    }

    public TreeMap<Integer, Weapon> getWeaponItems() {
        return weaponItems;
    }
}
