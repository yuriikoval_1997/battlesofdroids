package game_play.droids;

import game_play.armor.Armor;
import game_play.weapons.Weapon;
import graphics.ActionLoger;

public class BotEnemy extends Droid {
    private Armor armor;
    private Weapon weapon;

    public BotEnemy(String name) {
        super(name);
        incrementLvl();
    }

    public void shoot(Player target, String bodyPart, int damage) throws InterruptedException {
        ActionLoger actionLoger = new ActionLoger();
        if (target.healthPoints.getBodyPartHP(bodyPart) - damage < 0)
            damage = target.healthPoints.getBodyPartHP(bodyPart);
        if (target.getArmorItems().size() == 0) {
            target.healthPoints.setBodyPartHP(bodyPart, target.healthPoints.getBodyPartHP(bodyPart) - damage);
        } else if (target.getArmorItems().size() != 0) {
            double a = 0;
            int armor = 0;
            for (int i : target.getArmorItems().keySet()) {
                a += target.getArmorItems().get(i).getIntegrity() * target.getArmorItems().get(i).getImpact();
                target.getArmorItems().get(i).beDamaged(damage / 2);
            }
            armor = ((int) a) / target.getArmorItems().size();
            int is = (int) (((double) (100 - armor) / 100) * damage);
            //System.out.println(armor + " " + is); //WTF?
            target.healthPoints.setBodyPartHP(bodyPart,
                    target.healthPoints.getBodyPartHP
                            (bodyPart) - ((int) (((double) (100 - armor) / 100) * damage)));
            target.getArmorItems().entrySet().removeIf
                    (integerArmorEntry -> integerArmorEntry.getValue().getIntegrity() < 1);
        }
        actionLoger.printActionLoger(this.droidName + " has done " + damage + " damage to his target "
                + target.getDroidName() + ";");
    }

    public Weapon getWeapon() {
        return this.weapon;
    }

    public void giveWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    //TODO use getLvl() to make strength of bots dependant on lvl
}
