package game_play.droids;
import game_play.health.*;

import game_play.batteries.Battery;
import game_play.batteries.DroidBattery;
import graphics.ActionLoger;

public abstract class Droid {
    private static int lvl;
    protected String droidName;
    private Battery battery = new DroidBattery();
    public Health healthPoints = new Health();

    public Droid(String droidName){
        this.droidName = droidName;
    }

    public static void zeroLvl(){
        lvl = 0;
    }

    public static int getLvl() {
        return lvl;
    }

    public static void incrementLvl(){
        lvl++;
    }

    public int getHealthPoints() {
        return healthPoints.ofHead() + healthPoints.ofBody() + healthPoints.ofArms() + healthPoints.ofLegs();
    }

    public void hurt(int damage, String bodyPart) {
        this.healthPoints.setBodyPartHP(bodyPart ,this.healthPoints.getBodyPartHP(bodyPart)-damage);

    }

    public void shoot(Droid target, String bodyPart, int damage) throws InterruptedException {
        ActionLoger actionLoger = new ActionLoger();
        if(target.healthPoints.getBodyPartHP(bodyPart)-damage<0)
            damage=target.healthPoints.getBodyPartHP(bodyPart);
        target.healthPoints.setBodyPartHP(bodyPart ,target.healthPoints.getBodyPartHP(bodyPart)-damage);
        actionLoger.printActionLoger(this.droidName + " has done " + damage + " damage to his target " + target.getDroidName()+";");
    }

    public String getDroidName() {
        return droidName;
    }

    public Battery getBattery() {
        return this.battery;
    }

    @Override
    public String toString() {
        return droidName;
    }
}
