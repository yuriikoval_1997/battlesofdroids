package game_play.batteries;

public interface PowerSource {
    public void power(int energyConsumption) throws IllegalArgumentException;
}
