package game_play.batteries;

public interface Rechargeable {
    void recharge() throws InterruptedException;
}
