package game_play.batteries;

public interface Powerable {
    void extractEnergy(Battery battery);
}
