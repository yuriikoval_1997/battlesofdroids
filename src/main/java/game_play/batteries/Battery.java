package game_play.batteries;

/**
 * This class represent Battery which is used in all Droid objects.
 * @version 1.0
 * @author Yurii Koval
 */
public abstract class Battery implements Rechargeable, PowerSource{
    private int energy; /** holds int value from 0 to 100*/
    private String batteryName;

    Battery(String batteryName){
        this.energy = 100;
        this.batteryName = batteryName;
    }

    public int getEnergy() {
        return energy;
    }

    /**
     * This method discharge battery;
     * @param energyConsumption amount of energy needed to power something.
     * @throws IllegalArgumentException if energyConsumption is bigger than this.energy;
     */
    @Override
    public void power(int energyConsumption){
        this.energy -= energyConsumption;
        if(this.energy<0)
            this.energy=0;
    }

    /**
     * This method recharge getEnergy to 100% and print out percentage.
     * It takes 10 ms to recharge 1 percent.
     * @throws InterruptedException when current thread is interrupted.
     */
    //TODO - implementation can be redone to match console graphics.
    @Override
    public void recharge() throws InterruptedException {
        int fullCharge = 100;
        System.out.printf("%s is being recharged...", batteryName);
        System.out.println();
        for (int i = energy; i <= fullCharge; i++) {
            energy++;
            Thread.sleep(10);
            if (i % 10 == 0) {
                System.out.printf("%d%s", i, "%... ");
            }
        }
        System.out.println();
        System.out.println("Battery has been recharged.");
    }

    @Override
    public String toString() {
        return batteryName;
    }

    @Override
    public int hashCode() {
        return this.getClass().getSimpleName().hashCode();
    }
}
