package game_play.armor;

public abstract class Armor{
    protected int integrity;
    protected double impact;

    Armor(){
        this.integrity = 100;
    }

    public void beDamaged(int damage){
        this.integrity -=damage;
    }

    public int getIntegrity(){
        return integrity;
    }

    public double getImpact(){
        return impact;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    @Override
    public int hashCode() {
        return this.getClass().getSimpleName().hashCode();
    }
}
