package game_play.armor;

import game_play.batteries.Battery;
import game_play.batteries.Powerable;
import game_play.batteries.ShieldBattery;


public class EnergyShield extends Armor implements Powerable {
    private ShieldBattery shieldBattery;

    public EnergyShield(){
        //super();
        this.impact=0.95;
        this.shieldBattery = new ShieldBattery();
    }

    public ShieldBattery getShieldBattery(){
        return this.shieldBattery;
    }

    @Override
    public void extractEnergy(Battery battery) {
        //TODO
    }
}
