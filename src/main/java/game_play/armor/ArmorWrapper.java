package game_play.armor;

public class ArmorWrapper {
    private Armor armor;
    private int price;

    public ArmorWrapper(Armor armor, int price) {
        this.armor = armor;
        this.price = price;
    }

    public Armor getArmor() {
        return armor;
    }

    public int getPrice() {
        return price;
    }
}
